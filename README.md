# Motor

[![Build Status](https://travis-ci.org/0X1A/motor.svg?branch=master)](https://travis-ci.org/0X1A/motor)
[![Coverage Status](https://coveralls.io/repos/0X1A/motor/badge.png)](https://coveralls.io/r/0X1A/motor)

Motor is a 2D game engine using SDL2 that is still very early in development.

### Building the current tests

To build the current test bed you must first solve dependencies. They can be solved by running `./bin/motorwork --install`, which solves dependencies by building local copies. For more information see the bin [README](https://github.com/0x1A/motor/tree/master/bin).

```
$ qmake-qt4

$ make
```
