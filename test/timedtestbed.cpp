// Grossly verbose
#include "../include/assets.h"
#include "../include/window.h"
#include "../include/entity.h"
#include "../include/filesystem.h"
#include "../include/game.h"
#include "../include/globaltimer.h"
#include "../include/init.h"
#include "../include/input.h"
#include "../include/text.h"
#include "../include/parser.h"
#include "../include/player.h"
#include "../include/audio.h"

int main(int argc, char *argv[])
{
	WriteToLog();
	GlobalDebug(1);

	Init SYSINIT;
	Game TestGame;
	TestGame.Config("Motor Test Window", 1270, 720, "res/background.png");
	Filesystem TestSprites;
	TestSprites.SetArchive("res/testsprites.zip");
	Filesystem TestFail;
	TestFail.SetArchive("res/none.zip");
	TestFail.FileCheck("none.png");
	TestFail.OpenFile("none.png");

	Player TestPlayer;
	Entity TestEnt1;
	Entity TestEnt2;
	Asset Background;

	Background.LoadAsset(0, "background.png");
	Background.SetRects(20, 20, 0, 0);
	Background.DrawAsset("stretch");

	TestPlayer.LoadAsset(1, "testplayersprite_dleft.png");
	TestPlayer.SetRects(75, 45, 0, 0);
	TestPlayer.DrawAsset("");

	TestEnt1.LoadAsset(2, "testplayersprite_dright.png");
	TestEnt1.SetRects(75, 45, 100, 0);
	TestEnt1.DrawAsset("");

	TestEnt2.LoadAsset(3, "testplayersprite_down.png");
	TestEnt2.SetRects(75, 45, 100, 100);
	TestEnt2.DrawAsset("");

	Text Font;
	Font.SetFont(30, "res/test.ttf");
	Font.LoadText("Motor test", 255, 255, 255, 255);
	Font.DrawText();

	Audio MainAudio;
	MainAudio.CreateAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024);
	MainAudio.OpenAudioFile("KDE-Sys-Log-Out.ogg", 0);

	YML Config;
	Config.SetParseFile("public.yaml");

	GlobalTimer fps;
	fps.StartTimer();
	fps.PauseTimer();
	fps.StopTimer();

	std::string Args;
	int i;
	for (i = 1; i < argc; i++) {
		if (argv[i] != NULL)
			Args = argv[1];
		if (argv[i] == NULL) {
		}
		if (Args == "--timed") {
			SDL_Delay(3000);
		} else if (Args == "--test") {
			bool quit = false;
			while (quit == false) {
				while (PollEvent()) {
					if (((MainEvent.type == SDL_KEYDOWN) &&
								(MainEvent.key.keysym.sym == SDLK_ESCAPE)) ||
							(MainEvent.type == SDL_QUIT)) {
						quit = true;
						if (quit == true) {
						}
					}
				}
			}
		}
	}

	return 0;
}
