TARGET = MotorTest
DEPENDPATH += . include src
INCLUDEPATH += . include
include(motor-conf.pri)
CONFIG -= qt
LIBS += -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf -lyaml -lphysfs -lgcov
*-g++* {
	QMAKE_CXXFLAGS += -fprofile-arcs -ftest-coverage
}
QMAKE_CLEAN += *.gcda *.gcno *.txt *.Debug *.Release MotorTest -r release debug

# Input
HEADERS += include/assets.h \
           include/audio.h \
           include/entity.h \
           include/filesystem.h \
	   include/game.h \
           include/glob.h \
           include/globaltimer.h \
           include/init.h \
           include/input.h \
           include/parser.h \
           include/player.h \
           include/text.h \
           include/window.h
SOURCES += src/assets.cpp \
           src/audio.cpp \
           src/entity.cpp \
           src/filesystem.cpp \
	   src/game.cpp \
           src/glob.cpp \
           src/globaltimer.cpp \
           src/init.cpp \
           src/parser.cpp \
           src/player.cpp \
           src/text.cpp \
           src/window.cpp \
           test/timedtestbed.cpp
