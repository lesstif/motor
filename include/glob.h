#ifndef GLOB
#define GLOB

#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <fstream>

// Global SDL2 error print
extern void SDLErrorPrint();

// Global SDL2_image error print
extern void IMGErrorPrint();

// Writes stdout to log.txt in the binary's root directory.
// Replaces log.txt if exists.
extern void WriteToLog();

extern bool UserDebug(int SetUserDebug);

extern bool GlobalDebug(int SetGlobalDebug);

#endif
