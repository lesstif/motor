#ifndef INIT
#define INIT

#include "filesystem.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <physfs.h>

class Init : public Filesystem
{
public:
	int MixerFlags;

public:
	Init();
	~Init();
	bool InitSDL();
	void DeInitSDL();
	void DeInitSDLIMG();
	void DeInitSDLTTF();
	void DeInitSDLMIX();
	void DeInitPhysFS();
	int InitSDLIMG();
	int InitSDLTTF();
	int InitSDLMIX();
	int InitPhysFS();
};

#endif
