#include "filesystem.h"
#include <string>
#include <SDL.h>
#include <SDL_mixer.h>

class Audio : public Filesystem
{
public:
	SDL_AudioSpec get, got, wav;
	SDL_AudioDeviceID dev;
	Mix_Chunk *sample;
	const std::string AudioFile;
	const char *AudioDevName;
	int Freq, Channels, Chunk, Comp, i, count;
	Uint32 wav_len;
	Uint16 Form;
	Uint8 *wav_buf;
	Mix_Music *music;

public:
	Audio();
	~Audio();
	int OpenAudioFile(const std::string AudioFile, int Comp);
	const char *GetAudioDevName();
	int CloseAudioFile();
	int CreateAudio(int Freq, Uint16 Form, int Channels, int Chunk);
	int DestAudio();
};
