#ifndef INPUT
#define INPUT

#include <SDL.h>

SDL_Event MainEvent;

bool PollEvent()
{
	while (SDL_PollEvent(&MainEvent))
		;
	return true;
}

#endif
