#ifndef GLOBTIMER
#define GLOBTIMER

#include <SDL.h>

class GlobalTimer
{
private:
	int StartTick;
	int PausedTick;

	bool Started;
	bool Paused;

public:
	GlobalTimer();
	~GlobalTimer();
	void StartTimer();
	void StopTimer();
	void PauseTimer();
	void UnpauseTimer();

	int Get_Ticks();

	bool is_started();
	bool is_paused();
};

#endif
