#ifndef PARSER
#define PARSER

#include <yaml.h>
#include <string>

class YML
{
public:
	FILE oFile;
	yaml_parser_t Parser;
	yaml_token_t yEvent;
	const std::string YMLFile;

	YML();
	~YML();
	int SetParseFile(const std::string YMLFile);
	int SetParser();
	int SetToken();
	void DestParser();
	bool ParseInit();
};

#endif
