#ifndef ASSETS
#define ASSETS

#include "filesystem.h"
#include "window.h"
#include "glob.h"
#include <SDL.h>
#include <SDL_image.h>
#include <string>

class Asset : public Filesystem
{
public:
	SDL_Texture *AssetOpt;
	SDL_Surface *TempSurface;
	SDL_Surface *TempBackground;
	SDL_Rect SrcR;
	SDL_Rect DestR;
	SDL_Rect *SolidBackground;
	SDL_Rect Clips[];
	SDL_RWops *RW;
	const std::string File;
	std::string ImageManip;
	int AssetID;
	int Velocity;
	int AssetHeight;
	int AssetWidth;
	int AssetXOfst;
	int AssetYOfst;
	int ClipHeight;
	int ClipWidth;
	int Frames;
	Uint32 SolidColor;

public:
	Asset();
	~Asset();
	void DestTexture();
	void DrawAsset(std::string ImageManip);
	void DestAsset();
	int LoadAsset(int AssetID, const std::string File);
	int SetRects(int AssetHeight, int AssetWidth, int AssetXOfst, int AssetYOfst);
	int AssetStretch(int AssetHeight, int AssetWidth);
	int SetClip(int ClipHeight, int ClipWidth, int Frames);
	int DestSurface();
};

#endif
