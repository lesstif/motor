#ifndef GAME
#define GAME

#include "window.h"

class Game
{
	public:
		const std::string GameName, Icon;
		int Width, Height;
		Game();
		~Game();
		int Config(const std::string GameName, int Width, int Height, const std::string Icon);
};

#endif
