#ifndef TEXT
#define TEXT

#include "filesystem.h"
#include "window.h"
#include <SDL_ttf.h>
#include <SDL.h>

class Text : public Filesystem
{
public:
	SDL_Texture *FontOpt;
	SDL_Surface *FontSurface;
	SDL_Color FontColor;
	TTF_Font *T_Font;
	const std::string Font;
	const std::string PrintText;
	Uint8 fR, fG, fB, fA;
	int FontHeight;

public:
	Text();
	~Text();
	void DrawText();
	void DestFont();
	int LoadText(const std::string PrintText, Uint8 fR, Uint8 fG, Uint8 fB, Uint8 fA);
	int SetFont(int FontHeight, const std::string Font);
};

#endif
