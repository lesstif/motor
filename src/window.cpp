#include "glob.h"
#include "window.h"
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <fstream>

SDL_Window *window = NULL;
SDL_Renderer *w_renderer = NULL;

void NumDisplay()
{
	SDL_DisplayMode current;
	int i;
	for (i = 0; i < SDL_GetNumVideoDisplays(); i++) {
		int a = SDL_GetCurrentDisplayMode(i, &current);

		if (a != 0) {
			printf("Display: %d %s", i, SDL_GetError());
		} else {
			printf("Display: %d %d x %d @ %dHz\n", i, current.w, current.h, current.refresh_rate);
		}
	}
	if (SDL_GetNumVideoDisplays() <= 0) {
		printf("No displays available\n");
		SDLErrorPrint();
	}
}

int CreateWindow(int w_width, int w_height, const std::string WindowName)
{
	SDL_GetNumVideoDisplays();
	SDL_GetWindowDisplayIndex(window);

	window = SDL_CreateWindow(
	    WindowName.c_str(),
	    SDL_WINDOWPOS_UNDEFINED,
	    SDL_WINDOWPOS_UNDEFINED,
	    w_width,
	    w_height,
	    SDL_WINDOW_SHOWN);

	w_renderer = SDL_CreateRenderer(window, 0, SDL_RENDERER_ACCELERATED);
	if (window != NULL)
		printf("Window created\n");
	NumDisplay();
	return 0;
}

void SetWindowIcon(const std::string Icon)
{
	SDL_Surface *IconSurface = IMG_Load(Icon.c_str());
	SDL_SetWindowIcon(window, IconSurface);
	SDL_FreeSurface(IconSurface);
}

SDL_Renderer *GetRenderer()
{
	return w_renderer;
}

void DestRenderer()
{
	std::cout << "[Cleanup Started]\nRenderer destroyed\n";
	SDL_DestroyRenderer(w_renderer);
}

void DestWindow()
{
	std::cout << "Window destroyed\n";
	SDL_DestroyWindow(window);
}
