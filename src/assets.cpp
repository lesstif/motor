#include "assets.h"
#include <SDL.h>
#include <SDL_image.h>
#include <fstream>
#include <iostream>

Asset::Asset()
{
}

Asset::~Asset()
{
	DestAsset();
}

// Loads asset from archive
// @AssetID ID number for Asset
// @File File to be opened, fed to PhysFS and SDL
int Asset::LoadAsset(int AssetID, const std::string File)
{
	if (FileCheck(File.c_str()) == 1) {
		OpenFile(File.c_str());
		RW = SDL_RWFromMem(Data, Size);
		TempSurface = IMG_Load_RW(RW, 0);
		AssetOpt = SDL_CreateTextureFromSurface(GetRenderer(), TempSurface);
		SDL_FreeRW(RW);
		printf("AssetID: %d\tAsset loaded from compressed file\n", AssetID);
	}
	if (&AssetOpt == NULL) {
		std::cout << "AssetID: " << AssetID << "\tCould not load Asset\n";
		SDLErrorPrint();
		return -2;
	} else if (&AssetOpt != NULL) {
		std::cout << "AssetID: " << AssetID
			  << "\tAsset " << File << " loaded.\n";
		return 2;
	}
	return 0;
}

int Asset::DestSurface()
{
	if (&TempSurface != NULL) {
		SDL_FreeSurface(TempSurface);
		printf("AssetID: %d\tFreed surface\n", AssetID);
		return 1;
	} else if (&TempSurface == NULL) {
		printf("AssetID: %d\tNo surface to free\n", AssetID);
		SDLErrorPrint();
		return -1;
	}
	return 0;
}

// Asset dimensions
// @AssetHight @AssetWidth sets height and width
// @AssetXOfst @AssetYOfst sets Asset X and Y offsets on screen
int Asset::SetRects(int AssetHeight, int AssetWidth, int AssetXOfst, int AssetYOfst)
{
	SrcR.x = 0;
	SrcR.y = 0;
	SrcR.h = AssetHeight;
	SrcR.w = AssetWidth;
	DestR.x = AssetXOfst;
	DestR.y = AssetYOfst;
	DestR.h = AssetHeight;
	DestR.w = AssetWidth;
	return 0;
}

// Destroys texture created by LoadAsset
void Asset::DestTexture()
{
	if (AssetOpt != NULL) {
		SDL_DestroyTexture(AssetOpt);
		std::cout << "Texture " << File << " destroyed\n";
	} else if (AssetOpt == NULL) {
		std::cout << "No texture to destroy\n";
		SDLErrorPrint();
	}
}


void Asset::DrawAsset(std::string ImageManip)
{
	if (ImageManip == "Stretch" || ImageManip == "stretch") {
		SDL_RenderCopy(GetRenderer(), AssetOpt, NULL, NULL);
	} else if (ImageManip == "" || ImageManip == " ") {
		SDL_RenderCopy(GetRenderer(), AssetOpt, NULL, &DestR);
	}
	SDL_RenderPresent(GetRenderer());
}

void Asset::DestAsset()
{
	DestSurface();
	DestTexture();
	Filesystem::CloseFile();
}
