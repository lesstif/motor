#include "parser.h"
#include <iostream>
#include <fstream>

YML::YML()
{
}

YML::~YML()
{
	DestParser();
}

int YML::SetParseFile(const std::string YMLFile)
{
	FILE *oFile = fopen(YMLFile.c_str(), "r");
	SetParser();
	SetToken();
	if (!yaml_parser_initialize(&Parser)) {
		std::cout << "Failed to init parser\n";
		return -1;
	} else if (yaml_parser_initialize != NULL) {
		std::cout << "Parser init\n";
		return 1;
	}
	if (oFile == NULL) {
		std::cout << "Failed to open file:\t" << YMLFile << "\n";
		return -2;
	} else if (oFile != NULL) {
		std::cout << "File " << YMLFile << " opened successfully\n";
		yaml_parser_set_input_file(&Parser, oFile);
		return 2;
	}
	return 0;
}

int YML::SetParser()
{
	yaml_parser_t Parser;
	return 0;
}

int YML::SetToken()
{
	yaml_token_t Token;
	return 0;
}

void YML::DestParser()
{
	yaml_parser_delete(&Parser);
	std::cout << "Parser destroyed\n";
}
